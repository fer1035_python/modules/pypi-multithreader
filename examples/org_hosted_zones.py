#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""Crawl through AWS accounts."""
import sys
import json
import boto3
from botocore import exceptions
from multithreader import threads


__veersion__ = '0.1.0'


def list_accounts(
    access_key: str,
    secret_key: str,
    session_token: str,
    region_name: str
) -> list:
    """List all AWS accounts in the organization."""
    print("Getting accounts...")

    org = boto3.client(
        'organizations',
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        aws_session_token=session_token,
        region_name=region_name
    )

    paginator = org.get_paginator('list_accounts')
    accounts = []
    for page in paginator.paginate():
        for account in page['Accounts']:
            if account['Status'] == 'ACTIVE':
                accounts.append(account)
    
    print(f"Found {len(accounts)} active accounts...")

    return accounts


def get_credentials(
    access_key: str,
    secret_key: str,
    session_token: str,
    region_name: str,
    role_arn: str
) -> dict:
    """Get AWS organization Master account credentials with STS."""
    sts = boto3.client(
        'sts',
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        aws_session_token=session_token,
        region_name=region_name
    )

    sts_r = sts.assume_role(
        RoleArn=role_arn,
        RoleSessionName="aws-org-crawler"
    )

    return {
        'aws_access_key_id': sts_r["Credentials"]["AccessKeyId"],
        'aws_secret_access_key': sts_r["Credentials"]["SecretAccessKey"],
        'aws_session_token': sts_r["Credentials"]["SessionToken"],
        'region_name': region_name
    }


def list_hosted_zones(
    account_id: str,
    items: dict
) -> list:
    """Get all hosted zones in an AWS account."""
    print(f"Working on {account_id}...")

    zones = []
    try:
        credentials = get_credentials(
            items['access_key'],
            items['secret_key'],
            items['session_token'],
            items['region_name'],
            f'arn:aws:iam::{account_id}:role/{items["role_name"]}'
        )

        route53 = boto3.client(
            'route53',
            aws_access_key_id=credentials['aws_access_key_id'],
            aws_secret_access_key=credentials['aws_secret_access_key'],
            aws_session_token=credentials['aws_session_token'],
            region_name=credentials['region_name']
        )

        paginator = route53.get_paginator('list_hosted_zones')
        for page in paginator.paginate():
            zones.append(
                {
                    'account_id': account_id,
                    'hosted_zones': page['HostedZones']
                }
            )
    
    except exceptions.ClientError as e:
        zones.append(
            {
                'account_id': account_id,
                'hosted_zones': 'Could not assume role'
            }
        )

    return zones


def main():
    """Main function."""
    # Get arguments.
    if len(sys.argv) < 4 or len(sys.argv) > 7:
        print(
            f'Usage: python {sys.argv[0]}'
            ' <access_key>'
            ' <secret_key>'
            ' <session_token>'
            ' [<thread_num: 10>'
            ' <role_name: AWSRoute53ReadOnlyAccess>'
            ' <region_name: us-east-1>]'
        )
        sys.exit(1)

    access_key = sys.argv[1]
    secret_key = sys.argv[2]
    session_token = sys.argv[3]

    try:
        thread_num = int(sys.argv[4])
    except IndexError:
        thread_num = 10

    try:
        role_name = sys.argv[5]
    except IndexError:
        role_name = 'AWSRoute53ReadOnlyAccess'

    try:
        region_name = sys.argv[6]
    except IndexError:
        region_name = 'us-east-1'

    # Get account list.
    accounts = list_accounts(
        access_key,
        secret_key,
        session_token,
        region_name
    )
    account_ids = [account['Id'] for account in accounts]

    # Create multithreading arguments.
    items = {
        'access_key': access_key,
        'secret_key': secret_key,
        'session_token': session_token,
        'region_name': region_name,
        'role_name': role_name
    }

    # Execute multithreading calls to function.
    results = threads(
        list_hosted_zones,
        account_ids,
        items,
        thread_num=thread_num
    )

    # Organize results.
    zone_list = []
    for result in results:
        if len(result[0]['hosted_zones']) > 0:
            zone_list.append(result[0])
    zone_dict = {
        'zones': zone_list
    }

    # Write results to file.
    with open('zones.json', 'w') as f:
        json.dump(
            zone_dict,
            f,
            indent=2,
            default=str
        )


if __name__ == '__main__':
    main()
