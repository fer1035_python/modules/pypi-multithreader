#!/usr/bin/env bash

cd src
poetry build
poetry publish -u $PYPI_USERNAME -p $PYPI_PASSWORD
