#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Execute functions with multithreading.

Functions must accept the iterator, and other arguments as a dictionary.
See test_function() and main() for usage example.

Returns the list of results from all executions.
"""
import __init__

if __name__ == "__main__":
    __init__.main()
